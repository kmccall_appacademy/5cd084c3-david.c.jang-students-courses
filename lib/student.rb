class Student
  attr_accessor :first_name, :last_name, :courses
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  # should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
  def enroll(course)

    #conflicting_courses = @courses.select {|other_course| other_course.conflict_with?(course) }
    #if ! conflicting_courses
    #  course.add_student(self.name)
    #  @courses << course
    #end

    # I think the tests in student_spec is not smart.. forcing a brute force implementation
    # my above solution is a better implementation...
    return if @courses.include?(course)
    raise "course would cause conflict!" if has_conflict?(course)

    self.courses << course
    course.students << self
  end

  def has_conflict?(new_course)
    @courses.any? do |enrolled_course|
      new_course.conflicts_with?(enrolled_course)
    end
  end

  # return a hash of departments to # of credits the student is taking in that department.
  def course_load
    load = Hash.new(0)
    self.courses.each do |course|
      load[course.department] += course.credits
    end
    load
  end

end
